/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.academica.dao;

import br.com.senac.academica.model.Curso;


public class CursoDAO extends DAO<Curso> {
    
    public CursoDAO() {
        super( Curso.class);
    }
    
} 
